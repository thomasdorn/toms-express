import fs from 'fs'
import path from 'path'
import mongoose from "mongoose";

class RegisterModels {
    schema;
    constructor(schema: any) {
        this.schema = schema;
    }
    registerModels() {
            const files = fs.readdirSync(__dirname).filter(file =>
                (file.indexOf(".") !== 0) &&
                (file !== 'index.js') &&
                (file !== 'index.ts') &&
                (!file.includes("map")))
                for (const file of files) {
                    console.log('Registered Model Mongo: ', file.split('.')[0])
                    const filePath = path.join(__dirname, file);
                    const model = require(filePath);
                    const schema = model.default(this.schema)
                    mongoose.model(schema.name, schema.schema)
                }
            return mongoose
    }
}
export default new RegisterModels(mongoose.Schema);
