import * as mongoose from "mongoose"
import * as mongodb from "mongodb"

function userModel(Schema: any) {
    return {
        name: 'user',
        schema : new Schema({
            id: mongodb.ObjectId,
            username: String,
            firstname: String,
            lastname: String,
            password: String,
            email: String,
            token: String,
            loggedIn: Boolean,
        },{
            timestamps: true
        })
    }
}
export default userModel;
