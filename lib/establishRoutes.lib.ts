class EstablishRoutesLib {
    auth(spec: { [x: string]: { auth: any; }; }, route: string) {
        return spec[route].auth ? spec[route].auth : function (req: any, res: any, next: () => void) {
            next()
        }
    }
    middleWare(spec: { [x: string]: { middleware: any; }; }, route: string) {
        return spec[route].middleware ? spec[route].middleware : function (req: any, res: any, next: () => void) {
            next()
        }
    }
    establishRoutes(router: any, routeClass: any, pather: any) {
        const spec = routeClass.spec()
        for (let route in spec) {
            const auth = this.auth(spec, route);
            const middleware = this.middleWare(spec, route);
            if (spec[route].verb === 'GET' && spec.hasOwnProperty(route)) {
                let routePath = pather(route.toLowerCase());
                if (spec[route].routeOverride === true) {
                    routePath = route;
                }
                console.log('Registered route: ', routePath)
                router.get(
                    routePath,
                    [auth, middleware],
                    spec[route].handler
                )
            }
            if (spec[route].verb === 'POST' && spec.hasOwnProperty(route)) {
                let routePath = pather(route.toLowerCase());
                if (spec[route].routeOverride === true) {
                    routePath = route;
                }
                console.log('Registered route: ', routePath)
                router.post(
                    routePath,
                    [auth, middleware],
                    spec[route].handler
                )
            }
            if (spec[route].verb === 'DELETE' && spec.hasOwnProperty(route)) {
                let routePath = pather(route.toLowerCase());
                if (spec[route].routeOverride === true) {
                    routePath = route;
                }
                console.log('Registered route: ', routePath)
                router.delete(
                    routePath,
                    [auth, middleware],
                    spec[route].handler
                )
            }
            if (spec[route].verb === 'PATCH' && spec.hasOwnProperty(route)) {
                let routePath = pather(route.toLowerCase());
                if (spec[route].routeOverride === true) {
                    routePath = route;
                }
                if (Array.isArray(spec[route].routeVars)) {
                    const routeParams = spec[route].routeVars.join("/:");
                    routePath = routePath + "/:" + routeParams;
                }
                console.log('Registered route: ', routePath)
                router.patch(
                    routePath,
                    [auth, middleware],
                    spec[route].handler
                )
            }

        }
        return router
    }
}

export default new EstablishRoutesLib();
