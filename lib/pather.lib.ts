import path from 'path'

function getPrePendPath() {
    return '/api/v1';
}



const PatherServiceLib = {
    default(dirName: any) {
        const isWin = process.platform === "win32";
        if (isWin) {
            const dirPaths = dirName.split("/");
            const routePath = dirPaths.slice(dirPaths.length-2, dirPaths.length);
            const routeApiPath = `${getPrePendPath()}/${routePath[1]}`;
            return function(path: any) {
                return routeApiPath + `/${path}`;
            }
        } else {
            const dirPaths = dirName.split(path.sep);
            const routePath = dirPaths.slice(dirPaths.length-2, dirPaths.length);
            const routeApiPath = `${getPrePendPath()}/${routePath[1]}`;
            return function(path: any) {
                return routeApiPath + `/${path}`;
            }
        }
    }
}

export default PatherServiceLib
