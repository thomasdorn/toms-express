export const SYMBOL_SERVICE = {
    DatabaseService: Symbol.for("DatabaseService"),
    AuthService: Symbol.for("AuthService"),
    FormattersService: Symbol.for("FormattersService"),
    StartupEventServiceV2: Symbol.for("StartupEventServiceV2"),
    HttpServerService: Symbol.for("HttpServerService"),
}

export const SYMBOL_HANDLERS = {
    HelloWorldHandler: Symbol.for("HelloWorldHandler"),
}