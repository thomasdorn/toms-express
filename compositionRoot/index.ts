import {Container} from "inversify";
import {DatabaseService, IDatabaseService} from "../services/database.service";
import {AuthService, IAuthService} from "../services/auth.service";
import {HelloWorldHandler, IHelloWorldHandler} from "../routes/api/v1/helloworld/handler";
import {FormattersService, IFormattersService} from "../services/formatters.service";
import {SYMBOL_SERVICE, SYMBOL_HANDLERS} from "../types/symbols";
import {IStartupEventServiceV2, StartupEventServiceV2} from "../services/startupEvent.service";
import { HttpServerService, IHttpServerService } from "../services/httpserver.service";


export const myContainer = new Container();



export function bindContainer() {
    // services
    myContainer.bind<IDatabaseService>(SYMBOL_SERVICE.DatabaseService).to(DatabaseService).inSingletonScope();
    myContainer.bind<IAuthService>(SYMBOL_SERVICE.AuthService).to(AuthService).inSingletonScope();
    myContainer.bind<IFormattersService>(SYMBOL_SERVICE.FormattersService).to(FormattersService).inSingletonScope();
    myContainer.bind<IStartupEventServiceV2>(SYMBOL_SERVICE.StartupEventServiceV2).to(StartupEventServiceV2).inSingletonScope();
    myContainer.bind<IHttpServerService>(SYMBOL_SERVICE.HttpServerService).to(HttpServerService).inSingletonScope();




    // handlers
    myContainer.bind<IHelloWorldHandler>(SYMBOL_HANDLERS.HelloWorldHandler).to(HelloWorldHandler);



}
