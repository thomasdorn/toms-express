import mongoose from "mongoose"
import conf from "../config/conf"
import {inject, injectable} from "inversify";
import {IStartupEventServiceV2} from "./startupEvent.service";
import _ from "lodash";
import {SYMBOL_SERVICE} from "../types/symbols";
import registerModelsMongoose from "../models/index";


export interface IDatabaseService {
    mongoose: any;
    attachments: any;
    connection: any;
    db: any;
    dbDb: any;
    connectMongo: any;
}

@injectable()
export class DatabaseService implements IDatabaseService {
    connection = null;
    db: any
    dbDb: any;
    mongoose: any
    attachments: any;

    constructor(@inject(SYMBOL_SERVICE.StartupEventServiceV2) private startupEventService: IStartupEventServiceV2
    ) {
        this.connectMongo().then((db) => {
            // @ts-ignore
            this.db = db.connection;
            // @ts-ignore
            this.dbDb = db.db;
            // @ts-ignore
            this.mongoose = registerModelsMongoose.registerModels();
            this.startupEventService.startupSubject$.next(null)
        });
    }
    async connectMongo() {
        return mongoose.connect(conf.mongo.connString);
    }
}
