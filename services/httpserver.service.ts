import {Server, createServer} from "http"
import cluster from "cluster"
import process from "process"
import express from "express";
import { injectable } from "inversify";

const numCPUs = 1;

export interface IHttpServerService {
    cpus: number;
    port: number;
    host: string;
    default: Function;
    
}

@injectable()
export class HttpServerService implements IHttpServerService {
    cpus = 1;
    port = 4001;
    host = 'localhost';

    // @ts-ignore
    private httpServer: Server;

    constructor() {
        // @ts-ignore
        this.cpus = process.env.DEV_CPUS ? process.env.DEV_CPUS : numCPUs;

        console.log('dev cpus,', this.cpus);
    }
    createServer(app: express.Application) {
        this.httpServer = createServer(app)
    }
    listen(){
        console.log('server listening')
        this.httpServer.listen(Number(this.port))
    }
    default(app: express.Application) {
        if (Number(this.cpus) === 1) {
            this.createServer(app)
            this.listen();
        } else {
            if (cluster.isMaster) {
                console.log(`Master ${process.pid} is running`);
                for (let i = 0; i < this.cpus; i++) {
                    cluster.fork();
                }
                cluster.on("exit", (worker) => {
                    console.log(`Worker ${worker.process.pid} died`);
                    cluster.fork();
                });
            } else {
                console.log(`Worker ${process.pid} started`);
                this.createServer(app)
                this.listen();
            }
        }
    }


}
