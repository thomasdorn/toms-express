import {EventEmitter} from "events"
import {decorate, inject, injectable} from "inversify";
import {Subject, delay, map} from "rxjs";
decorate(injectable(), EventEmitter)

export interface IFormattersService {
    response$: Subject<any>;
}

@injectable()
export class FormattersService implements IFormattersService {
    public response$ = new Subject<any>()
    constructor() {
        this.response$.pipe(map((v: any) => {
            return v;
        })).subscribe(({result, classname, res}) => {
            const response = {
                success: true,
                [classname]: {
                    data: result
                }
            };
            res.send(response);
        });
    }
}
