import { injectable } from "inversify";
import { Subject } from "rxjs";

export interface IStartupEventServiceV2 {
    startupSubject$: Subject<any>;
}

@injectable()
export class StartupEventServiceV2 implements IStartupEventServiceV2 {
    public startupSubject$ = new Subject();
    constructor() {}
}