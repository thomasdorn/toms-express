import {DatabaseService, IDatabaseService} from "./database.service";
import jwt from 'jsonwebtoken';
import conf from '../config/conf';
import express from "express";
import {inject, injectable} from "inversify";

export interface IAuthService {
    generateCheckTokenFn: Function;
}

@injectable()
export class AuthService implements IAuthService {
    constructor(
        @inject(Symbol.for("DatabaseService")) private database: IDatabaseService
    ) {
    }
    generateCheckTokenFn() {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            try {
                next(new Error('issue with token'))
                const token = req.headers.token ?? req.query.token ?? req.body.token;
                // const result = await this.database.mongoose.model('user').findOne({
                //     token,
                //     loggedIn: true,
                //     code_validated: true,
                //     validation_code: null
                // }).exec();
                // if (!result?.username) {
                //     next(new Error('issue with token'))
                // }
                // await this.database.checkCurrentUserErrorUsername(result.username)
                // const verify: any = jwt.verify(result.token, conf.jwt.secretKey);

                // req.username  = result.username;
                // req.tokenPayload = verify
                // req.isLoggedIn = true

                // if (!token || !verify) {
                //     // res.status(500).send({
                //     //     message: "invalid auth"
                //     // })
                //     next(new Error('issue with token'))
                // } else if (result && verify && (token === result.token) && (verify.username === result.username)) {
                //     req.username = result.username;
                //     return next()
                // } else {
                //     // res.status(500).send({
                //     //     message: "invalid auth"
                //     // })
                // }
            } catch (e: any) {
                next(new Error(e.message));
            } finally {
                console.log('AuthBetaService::generateCheckTokenFn::checkToken')
            }
        }
    }
}
