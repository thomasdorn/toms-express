import express from 'express'
import bodyParser from "body-parser";
import morgan from "morgan";
import getRoutes from './routes/index';
import 'reflect-metadata';
import {bindContainer, myContainer} from "./compositionRoot";
import { IHttpServerService } from "./services/httpserver.service";
import {IStartupEventServiceV2} from "./services/startupEvent.service";
import {SYMBOL_SERVICE} from "./types/symbols";

bindContainer();

const startupService = myContainer.get<IStartupEventServiceV2>(SYMBOL_SERVICE.StartupEventServiceV2);
const httpServerService = myContainer.get<IHttpServerService>(SYMBOL_SERVICE.HttpServerService);


const main = express()
    .use(morgan('combined', {}))
    .use(bodyParser.json())
    .use(getRoutes())
    .use(errorHandler);


function errorHandler(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
    res.status(500).send(err.message)
}
startupService.startupSubject$.subscribe(() => {
    httpServerService.default(main)
})
process.on("uncaughtException", (err) => {
    console.log(err)
})
process.on("unhandledRejection", (err) => {
    console.log(err)
})
