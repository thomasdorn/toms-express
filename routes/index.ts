import express from 'express';


function getRoutes() {
    const Router = express.Router;
    return require("./createRoutes").default.map((v: any) => {
        return v()
    }).filter((v: any) => !!v).filter((router: any) => Object.getPrototypeOf(router) === Router)
        // @ts-ignore
    .reduce((rootRouter, router) => rootRouter.use(router), Router({mergeParms: true}));

}


export default getRoutes


