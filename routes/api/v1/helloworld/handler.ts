import express from "express"
import {inject, injectable} from "inversify";
import {IDatabaseService} from "../../../../services/database.service";
import {IAuthService} from "../../../../services/auth.service";
import {IFormattersService} from "../../../../services/formatters.service";


export interface IHelloWorldHandler {
    dirname: Function
}

@injectable()
export class HelloWorldHandler implements IHelloWorldHandler {
    checkToken;
    constructor(
        @inject(Symbol.for("DatabaseService")) private database: IDatabaseService,
        @inject(Symbol.for("AuthService")) private auth: IAuthService,
        @inject(Symbol.for('FormattersService')) private formatter: IFormattersService,
    ) {
        this.checkToken = auth.generateCheckTokenFn();
    }

    public dirname() {
        return __dirname
    }
    public spec() {
        return  {
            helloworld: {
                verb: 'GET',
                auth: undefined,
                handler: async (req: express.Request, res: express.Response) => {
                    try {
                        this.formatter.response$.next({
                            result: 'hello world',
                            classname: 'purchaseInfo',
                            res
                        })
                    } catch (e) {
                        console.error(e);
                    }
                }
            },
            "helloworld/:varone/:vartwo": {
                verb: 'GET',
                auth: this.checkToken,
                handler: async (req: express.Request, res: express.Response) => {
                    try {
                        this.formatter.response$.next({
                            result: req.params,
                            classname: 'purchaseInfo',
                            res
                        })
                    } catch (e) {
                        console.error(e);
                    }
                }
            },
        }
    }
}
