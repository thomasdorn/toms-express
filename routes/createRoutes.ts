// DO NOT EDIT see scripts/routes.ts
import {myContainer} from "../compositionRoot";
import establishRoutesLib from "../lib/establishRoutes.lib";
import express from 'express';
import pather from "../lib/pather.lib";
import fs from "fs";

import {IHelloWorldHandler} from "./api/v1/helloworld/handler";

const providers = [
   {interface: <IHelloWorldHandler>{}, stringname: 'HelloWorldHandler'}
]

export default providers.map((v, index) => {
   const _access = ()=> function() {
      v.interface
   }
   return function() {
      const routerClass: any = myContainer.get<ReturnType<typeof _access>>(Symbol.for(v.stringname))
      return establishRoutesLib.establishRoutes(
         express.Router({mergeParams: true}),
         routerClass,
         pather.default(`backend/routes/api/v1/${routerClass.dirname()}`))
      };
   });